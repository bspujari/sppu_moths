#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A program to download my Lepidoptera data and generate a checklist!

Created on Jun 16, 2021
Version : 1.0-16-06-2021 
@author: Bhalchandra Pujari 
@email : bspujari@scms.unipune.ac.in



"""




username="bspujari"
location="Ganeshkhind"


#--- MAIN CODE BEGIN NOW
import time,datetime
print ("--- Program Starts "+ datetime.datetime.now().strftime("%d/%B/%Y %H:%M")+"---")
print ("Initializing...")
from halo import Halo #spinnner for loop
import requests
import json
import math,random
import subprocess
import os.path,sys
import collections
from collections import defaultdict
import pandas as pd
from bokeh.plotting import figure
from bokeh.transform import cumsum
from bokeh.embed import autoload_static
from bokeh.layouts import row
from bokeh.resources import CDN






class iNat_leps():
    
    def main(self):
        
        
        self.name_rank_dict=defaultdict(list)
        self.phylodf=pd.DataFrame(columns=['Family','Tribe','Genus','Species'])
        self.taxa_id_dump()
        self.iNat_data=[] # empty list to save data
        #DF to create the checklist at the end
        self.checklist_df=pd.DataFrame(columns=['Observation ID','Family','Subfamily','Tribe','Genus','Species'])


        #print ('Downloading the data ... hang on..')
        self.parse_iNat() # fetch the data from iNat and save in above list
        
        #print ("Extracting the data..")
        self.extract_image_data() # extract the relevent data and 
        
    

        print ("Preparing checklist...")
        self.prepare_checklist()
        

        
        print ("Everything done. Please check 'SPPU_Moth_checklist.pdf'. Good bye.\n--- Program Ends "+ datetime.datetime.now().strftime("%d/%B/%Y %H:%M")+"---")
            
#-----MAIN ENDS HERE -----------------------------------


        
    def taxa_id_dump(self): 
        self.taxafile="taxa_id.csv" # a dump file to store taxa-id and names"
        self.TaxaId_Name=pd.DataFrame(columns=("TaxaID", "Name", "RankName", "RankLevel")) # a grand dict storing all IDs and corresponding names. 
        if os.path.isfile(self.taxafile): 
            #print ("Loaiding")
            self.TaxaId_Name =pd.read_csv(self.taxafile, \
                    names=("TaxaID", "Name", "RankName", "RankLevel")\
                    )
        else:
            print("Not exist")
            
        self.TaxaId_Name.astype({'TaxaID': 'int64'})
                    
        
        
    def extract_image_data(self):
        self.html_image_gallery="" # text to be appended in html file
        uniq_sci_name=[]
        spinner = Halo( text_color='blue',spinner='dots3')
        f=open(self.taxafile,'a')  # keep open the taxa_name file to append the unknonwn taxa_name pair
        self.total_observations=0
        spinner.start(text="Updating the database (takes looong at the first instance)")
        for entry in self.iNat_data  : #[:10]:
            
            for i in range(len(entry['results'])):
                if entry['results'][i]['place_guess'] and  location in entry['results'][i]['place_guess']: # Extract  data only from within SPPU
                    ancestry=[int(z) for z in entry['results'][i]['taxon']["min_species_ancestry"].split(",")]
                    if 47157 in ancestry and 47224 not in ancestry : # GET only Lepidoptera (taxa id=47157) excluding butterflies (47224)
                        self.total_observations+=1
                        obs_id=entry['results'][i]['id'] 
                        obs_url="https://www.inaturalist.org/observations/"+str(obs_id)
                        sci_name=entry['results'][i]['taxon']['name'] 
                        rank=entry['results'][i]['taxon']['rank'] # it is a string corresponding to rank_level ('genus'/'family' etc)
                        rank_level=entry['results'][i]['taxon']['rank_level']
                        # ^^^^^^^ what is the ID rank? Is is species or genus or family?
                        taxa_id=entry['results'][i]['taxon']['min_species_taxon_id'] # taxa ID. 
                        
                        thumb_url=entry['results'][i]['observation_photos'][0]['photo']['url'].split('?')[0]
                        original_url=thumb_url.replace("square","original")
                        
                        if rank_level == 10:
                            if sci_name not in uniq_sci_name: # 
                                uniq_sci_name.append(sci_name)
                                

                        self.checklist_df=self.checklist_df.append({'Observation ID':int(obs_id)},ignore_index=True)


                        for ancester in ancestry :
                            # This loop of the program is terrible. For every
                            # new entry it sends a query to iNat server. May be
                            # there is better way to dump all the data for taxa
                            # and taxaid beforehand? Keeping it as it is, but
                            # needs to revamp.
                            
                            if len(self.TaxaId_Name[self.TaxaId_Name["TaxaID"]==ancester]) == 0: # means no entry for ancester is found in database
                                api_string="http://api.inaturalist.org/v1/taxa/"+str(ancester)
                                r=requests.get(api_string) # download .. make sure proxies are fine
                                try:
                                    r=json.loads(r.text)
                                except :
                                    print ("Error:", ancester, r)
                                taxaname=r['results'][0]['name']                                    
                                rank=r['results'][0]['rank']
                                rank_level=r['results'][0]['rank_level']
                                newrow={"TaxaID":ancester, "Name":taxaname, "RankName":rank, "RankLevel":rank_level}
                                self.TaxaId_Name=self.TaxaId_Name.append(newrow, ignore_index=True)                                
                                f.write(str(ancester)+","+taxaname+","+str(rank) +"," + str(rank_level)+"\n")
                            else:
                                rank=self.TaxaId_Name[self.TaxaId_Name['TaxaID']==ancester]['RankName'].values[0]
                                rank_level=self.TaxaId_Name[self.TaxaId_Name['TaxaID']==ancester]['RankLevel'].values[0]
                                taxaname=self.TaxaId_Name[self.TaxaId_Name['TaxaID']==ancester]['Name'].values[0]
                            

                            self.name_rank_dict[rank].append(taxaname)       


                            # update checklist df
                            if rank.title() in self.checklist_df.columns : 
                                self.checklist_df.loc[self.checklist_df['Observation ID']==int(obs_id),rank.title()]=taxaname
                            
                        
                        
        f.close()
        spinner.succeed("Extraction done")
    
    def prepare_checklist(self):
        self.checklist_df['Observation ID']=self.checklist_df['Observation ID'].astype('int')  
        self.checklist_df.dropna(subset=['Family'],inplace=True)
        self.checklist_df=self.checklist_df.sort_values(by=self.checklist_df.columns.to_list()[1:]) # sort but skip 'Observation ID' column
        self.checklist_df=self.checklist_df.drop_duplicates(subset=self.checklist_df.columns.to_list()[1:])
        self.checklist_df=self.checklist_df.reset_index()
        self.checklist_df.index += 1 
        f=open("checklist.tex",'w')
        f.write(self.checklist_df.to_latex(columns=['Family','Subfamily','Genus','Species','Observation ID'],longtable=True,na_rep=' '))
        f.close()
        print ("Compiling the checklist. You need functioning LaTeX environment\n")
        subprocess.run(["pdflatex","SPPU_Moth_checklist",">","/dev/null"])
        print ("Please check SPPU_Moth_checklist.pdf file for the checklist!")


            

    def parse_iNat(self): 
      
      page_count=1 # Required by the format of API 
           # total observations = pages * observations per page
           #                  (set to 100 at this stage)
      total_pages='NA'
      
      
      
      spinner = Halo( text_color='green',spinner='circleQuarters')
      t0=time.time()
      while True:
      #while page_count <2:
          
          if page_count == 1 : 
              spin_text="Downloading page "+str(page_count)
          else :
              spin_text="Downloading page "+str(page_count)+" of "+ total_pages+\
              "  Time Remaining : " + str(math.ceil((t1-t0)*(int(total_pages)-page_count+1))) + " sec"
          spinner.start(text=spin_text )
          

              # Build the string for API download
          api_string="http://api.inaturalist.org/v1/observations?"+\
          "user_id="+username+\
          "&identified=true"+\
          "&photos=true"+\
          "&page="+str(page_count)+\
          "&per_page=100"+\
          "&order=desc"+\
          "&order_by=created_at"
          # looks like 100 is the limit for per page observations.
          
          try:
              r=requests.get(api_string) # download .. make sure proxies are fine
          except :
              print ("\nERROR: Active internet connection is required to download the data. Please check you connection and try again.\nI have to stop here. Bye.")
              sys.exit()
          if not r : 
              print ("\nERROR! Did not receive any data. Please check your network connectivity. Are you behind firewall/proxy?")
              sys.exit()
              
          j=json.loads(r.text)
          
          
          if page_count == 1 : # for the first iteration, determine how many pages of downloadng is there..
              total_pages=str(math.ceil(j['total_results']/100) +1) # total results/results per page
              t1=time.time()
              
          if j['results'] : # error will occur when page numbers are exhausted.. till that time keep increasing page numbers.
              page_count+=1
              self.iNat_data.append(json.loads(r.text))
          else:
              break
          
          
          spinner.stop()
      spinner.succeed("Download complete") 
if __name__ == '__main__':
   u=iNat_leps()
   u.main()
